package ru.kharlamova.tm.api;

import ru.kharlamova.tm.model.Task;

public interface ITaskController {

    void showList();

    void create();

    void clear();

    void showTaskByIndex();

    void showTask(Task task);

    void showTaskByName();

    void showTaskById();

    void removeTaskByIndex();

    void removeTaskByName();

    void removeTaskById();

    void  updateTaskByIndex();

    void  updateTaskById();

    void startTaskById();

    void startTaskByIndex();

    void startTaskByName();

    void finishTaskById();

    void finishTaskByIndex();

    void finishTaskByName();

    void changeTaskStatusById();

    void changeTaskStatusByIndex();

    void changeTaskStatusByName();

}
