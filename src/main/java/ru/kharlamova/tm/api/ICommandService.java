package ru.kharlamova.tm.api;

import ru.kharlamova.tm.model.Command;

public interface ICommandService {

    Command[] getTerminalCommands();

}
