package ru.kharlamova.tm.repository;

import ru.kharlamova.tm.api.ICommandRepository;
import ru.kharlamova.tm.constant.ArgumentConst;
import ru.kharlamova.tm.constant.TerminalConst;
import ru.kharlamova.tm.model.Command;

public class  CommandRepository implements ICommandRepository {

    private static final Command ABOUT = new Command(
            TerminalConst.CMD_ABOUT, ArgumentConst.ARG_ABOUT, "Show developer info."
    );

    private static final Command HELP = new Command(
            TerminalConst.CMD_HELP, ArgumentConst.ARG_HELP, "Show terminal commands."
    );

    private static final Command VERSION = new Command(
            TerminalConst.CMD_VERSION, ArgumentConst.ARG_VERSION, "Show application version."
    );

    private static final Command INFO = new Command(
            TerminalConst.CMD_INFO, ArgumentConst.ARG_INFO, "Show system info."
    );

    private static final Command EXIT = new Command(
            TerminalConst.CMD_EXIT, null, "Close application."
    );

    private static final Command ARGUMENTS = new Command(
            TerminalConst.CMD_ARGUMENTS, null, "Show program arguments."
    );

    private static final  Command COMMANDS = new Command(
            TerminalConst.CMD_COMMANDS, null, "Show program commands."
    );

    private static final  Command TASK_CREATE = new Command(
            TerminalConst.CMD_TASK_CREATE, null, "Create new task."
    );

    private static final  Command TASK_CLEAR = new Command(
            TerminalConst.CMD_TASK_CLEAR, null, "Clear all tasks."
    );

    private static final  Command TASK_LIST = new Command(
            TerminalConst.CMD_TASK_LIST, null, "Show task list."
    );

    private static final Command TASK_VIEW_BY_NAME = new Command(
            TerminalConst.CMD_TASK_VIEW_BY_NAME, null, "View task by name."
    );

    private static final Command TASK_VIEW_BY_ID = new Command(
            TerminalConst.CMD_TASK_VIEW_BY_ID, null, "View task by id."
    );

    private static final Command TASK_VIEW_BY_INDEX = new Command(
            TerminalConst.CMD_TASK_VIEW_BY_INDEX, null, "View task by index."
    );

    private static final Command TASK_REMOVE_BY_NAME = new Command(
            TerminalConst.CMD_TASK_REMOVE_BY_NAME, null, "Remove task by name."
    );

    private static final Command TASK_REMOVE_BY_ID = new Command(
            TerminalConst.CMD_TASK_REMOVE_BY_ID, null, "Remove task by id."
    );

    private static final Command TASK_REMOVE_BY_INDEX = new Command(
            TerminalConst.CMD_TASK_REMOVE_BY_INDEX, null, "Remove task by index."
    );

    private static final Command TASK_UPDATE_BY_ID = new Command(
            TerminalConst.CMD_TASK_UPDATE_BY_ID, null, "Update task by id."
    );

    private static final Command TASK_UPDATE_BY_INDEX = new Command(
            TerminalConst.CMD_TASK_UPDATE_BY_INDEX, null, "Update task by index."
    );

    private static final Command TASK_START_BY_ID = new Command(
            TerminalConst.CMD_TASK_START_BY_ID, null, "Start task by id."
    );

    private static final Command TASK_START_BY_INDEX = new Command(
            TerminalConst.CMD_TASK_START_BY_INDEX, null, "Start task by index."
    );

    private static final Command TASK_START_BY_NAME = new Command(
            TerminalConst.CMD_TASK_START_BY_NAME, null, "Start task by name."
    );

    private static final Command TASK_FINISH_BY_ID = new Command(
            TerminalConst.CMD_TASK_FINISH_BY_ID, null, "Finish task by id."
    );

    private static final Command TASK_FINISH_BY_INDEX = new Command(
            TerminalConst.CMD_TASK_FINISH_BY_INDEX, null, "Finish task by index."
    );

    private static final Command TASK_FINISH_BY_NAME = new Command(
            TerminalConst.CMD_TASK_FINISH_BY_NAME, null, "Finish task by name."
    );

    private static final Command TASK_CHANGE_STATUS_BY_ID = new Command(
            TerminalConst.CMD_TASK_CHANGE_STATUS_BY_ID, null, "Change task by id."
    );

    private static final Command TASK_CHANGE_STATUS_BY_INDEX = new Command(
            TerminalConst.CMD_TASK_CHANGE_STATUS_BY_INDEX, null, "Change task by index."
    );

    private static final Command TASK_CHANGE_STATUS_BY_NAME = new Command(
            TerminalConst.CMD_TASK_CHANGE_STATUS_BY_NAME, null, "Change task by name."
    );

    private static final  Command PROJECT_CREATE = new Command(
            TerminalConst.CMD_PROJECT_CREATE, null, "Create new project."
    );

    private static final  Command PROJECT_CLEAR = new Command(
            TerminalConst.CMD_PROJECT_CLEAR, null, "Clear all projects."
    );

    private static final  Command PROJECT_LIST = new Command(
            TerminalConst.CMD_PROJECT_LIST, null, "Show project list."
    );

    private static final Command PROJECT_VIEW_BY_NAME = new Command(
            TerminalConst.CMD_PROJECT_VIEW_BY_NAME, null, "View project by name."
    );

    private static final Command PROJECT_VIEW_BY_ID = new Command(
            TerminalConst.CMD_PROJECT_VIEW_BY_ID, null, "View project by id."
    );

    private static final Command PROJECT_VIEW_BY_INDEX = new Command(
            TerminalConst.CMD_PROJECT_VIEW_BY_INDEX, null, "View project by index."
    );

    private static final Command PROJECT_REMOVE_BY_NAME = new Command(
            TerminalConst.CMD_PROJECT_REMOVE_BY_NAME, null, "Remove project by name."
    );

    private static final Command PROJECT_REMOVE_BY_ID = new Command(
            TerminalConst.CMD_PROJECT_REMOVE_BY_ID, null, "Remove project by id."
    );

    private static final Command PROJECT_REMOVE_BY_INDEX = new Command(
            TerminalConst.CMD_PROJECT_REMOVE_BY_INDEX, null, "Remove project by index."
    );

    private static final Command PROJECT_UPDATE_BY_ID = new Command(
            TerminalConst.CMD_PROJECT_UPDATE_BY_ID, null, "Update project by id."
    );

    private static final Command PROJECT_UPDATE_BY_INDEX = new Command(
            TerminalConst.CMD_PROJECT_UPDATE_BY_INDEX, null, "Update project by index."
    );

    private static final Command PROJECT_START_BY_ID = new Command(
            TerminalConst.CMD_PROJECT_START_BY_ID, null, "Start project by id."
    );

    private static final Command PROJECT_START_BY_INDEX = new Command(
            TerminalConst.CMD_PROJECT_START_BY_INDEX, null, "Start project by index."
    );

    private static final Command PROJECT_START_BY_NAME = new Command(
            TerminalConst.CMD_PROJECT_START_BY_NAME, null, "Start project by name."
    );

    private static final Command PROJECT_FINISH_BY_ID = new Command(
            TerminalConst.CMD_PROJECT_FINISH_BY_ID, null, "Finish project by id."
    );

    private static final Command PROJECT_FINISH_BY_INDEX = new Command(
            TerminalConst.CMD_PROJECT_FINISH_BY_INDEX, null, "Finish project by index."
    );

    private static final Command PROJECT_FINISH_BY_NAME = new Command(
            TerminalConst.CMD_PROJECT_FINISH_BY_NAME, null, "Finish project by name."
    );

    private static final Command PROJECT_CHANGE_STATUS_BY_ID = new Command(
            TerminalConst.CMD_PROJECT_CHANGE_STATUS_BY_ID, null, "Change project by id."
    );

    private static final Command PROJECT_CHANGE_STATUS_BY_INDEX = new Command(
            TerminalConst.CMD_PROJECT_CHANGE_STATUS_BY_INDEX, null, "Change project by index."
    );

    private static final Command PROJECT_CHANGE_STATUS_BY_NAME = new Command(
            TerminalConst.CMD_PROJECT_CHANGE_STATUS_BY_NAME, null, "Change project by name."
    );

    private static final Command[] TERMINAL_COMMANDS = new Command[] {
            ABOUT, HELP, VERSION, INFO, ARGUMENTS, COMMANDS,

            TASK_CLEAR, TASK_CREATE, TASK_LIST, TASK_VIEW_BY_NAME, TASK_VIEW_BY_ID, TASK_VIEW_BY_INDEX,
            TASK_REMOVE_BY_NAME, TASK_REMOVE_BY_ID, TASK_REMOVE_BY_INDEX,
            TASK_UPDATE_BY_ID, TASK_UPDATE_BY_INDEX,
            TASK_START_BY_ID, TASK_START_BY_INDEX, TASK_START_BY_NAME,
            TASK_FINISH_BY_ID, TASK_FINISH_BY_INDEX, TASK_FINISH_BY_NAME,
            TASK_CHANGE_STATUS_BY_ID, TASK_CHANGE_STATUS_BY_INDEX, TASK_CHANGE_STATUS_BY_NAME,

            PROJECT_CREATE, PROJECT_CLEAR, PROJECT_LIST, PROJECT_VIEW_BY_NAME, PROJECT_VIEW_BY_ID, PROJECT_VIEW_BY_INDEX,
            PROJECT_REMOVE_BY_NAME, PROJECT_REMOVE_BY_ID, PROJECT_REMOVE_BY_INDEX,
            PROJECT_UPDATE_BY_ID, PROJECT_UPDATE_BY_INDEX,
            PROJECT_START_BY_ID, PROJECT_START_BY_INDEX, PROJECT_START_BY_NAME,
            PROJECT_FINISH_BY_ID, PROJECT_FINISH_BY_INDEX, PROJECT_FINISH_BY_NAME,
            PROJECT_CHANGE_STATUS_BY_ID, PROJECT_CHANGE_STATUS_BY_INDEX, PROJECT_CHANGE_STATUS_BY_NAME,

            EXIT
    };

    public Command[] getTerminalCommands() {
        return TERMINAL_COMMANDS;
    }

}
